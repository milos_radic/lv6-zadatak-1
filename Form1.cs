﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zadatak1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {//minus
            double a,b;
            if(textBox1.Text != "" && textBox2.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a) && double.TryParse(textBox2.Text, out b))
                {
                    rjesenje.Text = (a - b).ToString();
                }
                else
                {
                    MessageBox.Show("Jedno ili oba polja su neispravno unijeta.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Jedno ili oba polja su prazna.", "Prazno polje");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {//korijen
            double a;
            if (textBox1.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a))
                {
                    if (a >= 0)
                    {
                        rjesenje.Text = Math.Sqrt(a).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Pod korijenom treba biti broj veći ili jednak nuli", "Greška");
                    }
                }
                else
                {
                    MessageBox.Show("Polje je neispravno unijeto.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Polje je prazno.", "Prazno polje");
            }
        }

        private void button_log10_Click(object sender, EventArgs e)
        {//log10
            double a;
            if (textBox1.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a))
                {
                    if (a > 0)
                    {
                        rjesenje.Text = Math.Log10(a).ToString();
                    }
                    else
                    {
                        MessageBox.Show("logaritam se može odrediti samo od pozitivnog broja", "Greška");
                    }
                }
                else
                {
                    MessageBox.Show("Polje je neispravno unijeto.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Polje je prazno.", "Prazno polje");
            }
        }

        private void button_cos_Click(object sender, EventArgs e)
        {//cos
            double a;
            if (textBox1.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a))
                {
                    rjesenje.Text = Math.Cos(a).ToString();
                }
                else
                {
                    MessageBox.Show("Polje je neispravno unijeto.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Polje je prazno.", "Prazno polje");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {//clear
            textBox1.Text = "";
            textBox2.Text = "";
            rjesenje.Text = "";
        }

        private void button1_Click_2(object sender, EventArgs e)
        {//quit
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {//rezultat

        }

        private void label1_Click_1(object sender, EventArgs e)
        {//rjesenje

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_3(object sender, EventArgs e)
        {//plus
            double a, b;
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a) && double.TryParse(textBox2.Text, out b))
                {
                    rjesenje.Text = (a + b).ToString();
                }
                else
                {
                    MessageBox.Show("Jedno ili oba polja su neispravno unijeta.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Jedno ili oba polja su prazna.", "Prazno polje");
            }
        }

        private void button_mnozenje_Click(object sender, EventArgs e)
        {
            double a, b;
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a) && double.TryParse(textBox2.Text, out b))
                {
                    rjesenje.Text = (a * b).ToString();
                }
                else
                {
                    MessageBox.Show("Jedno ili oba polja su neispravno unijeta.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Jedno ili oba polja su prazna.", "Prazno polje");
            }
        }

        private void button_dijeljenje_Click(object sender, EventArgs e)
        {
            double a, b;
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a) && double.TryParse(textBox2.Text, out b))
                {
                    if (b != 0)
                    {
                        rjesenje.Text = (a/b).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Djeljitelj ne može biti nula", "Greška");
                    }
                }
                else
                {
                    MessageBox.Show("Jedno ili oba polja su neispravno unijeta.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Jedno ili oba polja su prazna.", "Prazno polje");
            }
        }

        private void button_ln_Click(object sender, EventArgs e)
        {
            double a;
            if (textBox1.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a))
                {
                    if (a > 0)
                    {
                        rjesenje.Text = Math.Log(a).ToString();
                    }
                    else
                    {
                        MessageBox.Show("logaritam se može odrediti samo od pozitivnog broja", "Greška");
                    }
                }
                else
                {
                    MessageBox.Show("Polje je neispravno unijeto.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Polje je prazno.", "Prazno polje");
            }
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            double a;
            if (textBox1.Text != "")
            {
                if (double.TryParse(textBox1.Text, out a))
                {
                    rjesenje.Text = Math.Sin(a).ToString();
                }
                else
                {
                    MessageBox.Show("Polje je neispravno unijeto.", "Greška");
                }
            }
            else
            {
                MessageBox.Show("Polje je prazno.", "Prazno polje");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = rjesenje.Text;
            textBox2.Text = "";
        }
    }
}
